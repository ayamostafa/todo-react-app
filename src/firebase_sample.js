import firebase from "firebase/app";
import("firebase/firestore");


// Note Rename this file to firebase.js and put your firebase app config
const firebaseConfig = {
    apiKey: "your_api_key",
    authDomain: "todo-react-app-fb30b.firebaseapp.com",
    projectId: "todo-react-app-fb30b",
    storageBucket: "todo-react-app-fb30b.appspot.com",
    messagingSenderId: "833551486992",
    appId: "your_app_id"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();


export default db;